package com.iaascloud;

import java.io.IOException;

import com.iaascloud.compute.Initializer;
import com.iaascloud.compute.Providers;
import com.iaascloud.compute.drivers.MigrateDriver;
import com.iaascloud.compute.drivers.ProviderDriver;
import com.iaascloud.compute.instances.Instance;
import com.iaascloud.deploy.SSH_To_Instance;

public class MigrateExample {

	/**
	 * @param args
	 */
	
	public static void main(String[] args) throws Exception, IOException, InterruptedException {

		/*
		 * Credentials Details
		 */
		
		String credentialsDetails = 
				"{"
				+ "\"keypath\" : \"/opt/stack/projecta/iaas-cloud-middleware/src/AwsCredentials.properties\","
				+ "\"endpoint\" : \"us-west-2\""
				+ "\"username\" : \"admin\","
				+ "\"password\" : \"123\""
				+ "\"authurl\" : \"http://192.168.0.100\""
				+ "}"; 
		
		Initializer ourInitializer = new Initializer(Providers.AMAZON, credentialsDetails); 
		ProviderDriver ourDriver = ourInitializer.getDriver();
		
		Initializer newInitializer = new Initializer(Providers.OPENSTACK, credentialsDetails); 
		ProviderDriver newDriver = newInitializer.getDriver();
		
		
		//newDriver.describeAllInstances();
		Instance ourInstance = ourDriver.getInstance("i-0482dd0c");
		
		MigrateDriver mig = new MigrateDriver(ourDriver,newDriver, ourInstance);
		Instance migratedInstance = mig.migrate();
	
		//System.out.println(migratedInstance);
		
	}

}
