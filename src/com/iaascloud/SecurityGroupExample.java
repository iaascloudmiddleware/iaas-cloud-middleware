package com.iaascloud;

import java.io.IOException;

import com.iaascloud.compute.Initializer;
import com.iaascloud.compute.Providers;
import com.iaascloud.compute.drivers.ProviderDriver;
import com.iaascloud.compute.instances.Instance;

public class SecurityGroupExample {

	public static void main(String[] args) throws IOException, InterruptedException, Exception {
		// TODO Auto-generated method stub
		String initialDetailsAmazon = 
				"{"
				+ "\"keypath\" : \"/Users/vivek/Documents/workspace/IaasCloud/src/AwsCredentials.properties\","
				+ "\"endpoint\" : \"us-west-2\""
				+ "}"; 
		Initializer amazonIntializer = new Initializer(Providers.AMAZON, initialDetailsAmazon); 
		ProviderDriver amazonDriver = amazonIntializer.getDriver();
		String groupId = amazonDriver.createSecurityGroup("Shankar_Girl");
		Instance tempInstance = amazonDriver.getInstance("i-af0c50a7");
		amazonDriver.associateSecurityGroupWithInstance(groupId, tempInstance);
	}

}
