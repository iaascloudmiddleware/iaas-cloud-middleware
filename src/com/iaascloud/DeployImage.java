package com.iaascloud;

import java.io.IOException;

import com.iaascloud.compute.Initializer;
import com.iaascloud.compute.Providers;
import com.iaascloud.compute.drivers.ProviderDriver;
import com.iaascloud.compute.instances.Instance;

public class DeployImage {
	public static void main(String[] args) throws Exception, IOException, InterruptedException{
		
		/*
		 * Initial Credentials Details
		 */
		
		String credentialsDetails = 
				"{"
				+ "\"keypath\" : \"/opt/stack/projecta/iaas-cloud-middleware/src/AwsCredentials.properties\","
				+ "\"endpoint\" : \"us-west-2\""
				+ "\"username\" : \"admin\","
				+ "\"password\" : \"123\""
				+ "\"authurl\" : \"http://192.168.0.100\""
				+ "}"; 
		Initializer ourInitializer = new Initializer(Providers.OPENSTACK, credentialsDetails); 
		ProviderDriver ourDriver = ourInitializer.getDriver();
		
		String deployImage = 
				"{"
				+ "\"mode\" : \"local\""
				+ "\"bucket\" : \"iaasmiddleware\""
				+ "\"path\" : \"/opt/stack/Images/Hadoop.vhd\""
				+ "\"name\": \"Hadoop_OS\","
				+ "\"architecture\" : \"x86_64\""
				+ "\"mindisk\": \"8\","
				+ "\"minram\": \"2048\","
				+ "\"format\" : \"VHD\""
				+ "\"platform\" : \"Linux\""
				+ "}";
		
		Instance ourInstance = ourDriver.importInstance(deployImage);
		System.out.println(ourInstance);
		/*
		 // Create an instance from an already uploaded image to S3 bucket
		  
		 String deployImageAmazon = 
				"{"
				+ "\"mode\" : \"remote\""
				+ "\"bucket\" : \"iaasmiddleware\""
				+ "\"path\" : \"<manifest.xml path>\""
				+ "\"architecture\" : \"x86_64\""
				+ "\"mindisk\": \"8\","
				+ "\"minram\": \"2048\","
				+ "\"format\" : \"VHD\""
				+ "\"platform\" : \"Linux\""
				+ "}";
		
		 
		 */
		
	}
}
