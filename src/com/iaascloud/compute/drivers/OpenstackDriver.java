package com.iaascloud.compute.drivers;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.iaascloud.compute.instances.Instance;
import com.iaascloud.compute.instances.OpenstackInstance;
import com.iaascloud.compute.volumes.Volume;
import com.iaascloud.deploy.DeployApp;

public class OpenstackDriver implements ProviderDriver {
	private String userName;
	private String password;
	private String authUrl;
	private String computeUrl;
	private String authenticationToken;
	private String tenantId;
	private String middlewareId = "";
	public static HashMap<String, Instance> openstackInstanceMap = new HashMap<String, Instance>();

	private void setExistingInstances() {
		

		try {
			String instanceUrl = computeUrl + "/" + tenantId
					+ "/servers/detail";
			HttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(instanceUrl);
			httpGet.setHeader("X-Auth-Project-Id", userName);
			httpGet.setHeader("Accept", "application/json");
			httpGet.setHeader("User-Agent", "python-novaclient");
			httpGet.setHeader("Content-Type", "application/json");
			httpGet.setHeader("X-Auth-Token", authenticationToken);
			HttpResponse httpResponse = httpClient.execute(httpGet);

			BufferedReader rd = new BufferedReader(new InputStreamReader(
					httpResponse.getEntity().getContent()));
			String tempLine = "";
			StringBuffer line = new StringBuffer();
			while ((tempLine = rd.readLine()) != null) {
				line.append(tempLine);
				// System.out.println(line);

			}
			Object obj = JSONValue.parse(line.toString());
			JSONObject jsonObject = (JSONObject) obj;
			JSONArray instances = (JSONArray) jsonObject.get("servers");
			//System.out.println(instances);
			for (int i = 0; i < instances.size(); i++) {
				JSONObject tempServers = (JSONObject) instances.get(i);
				String instanceId = (String) tempServers.get("id");
				String instanceState = (String) tempServers.get("status");
				String instanceName = (String) tempServers.get("name");
				JSONObject tempImageId = (JSONObject) tempServers.get("image");
				String imageId = (String) tempImageId.get("id"); 
				JSONObject tempAddresses = (JSONObject) tempServers.get("addresses");
				JSONArray tempPrivate = (JSONArray) tempAddresses.get("private");
				String IPAddress = (String) ((JSONObject)tempPrivate.get(0)).get("addr");

				OpenstackInstance tempInstance = new OpenstackInstance(); 
				tempInstance.setImageId(imageId);
				tempInstance.setInstanceId(instanceId);
				tempInstance.setInstanceIP(IPAddress);
				tempInstance.setInstanceName(instanceName);
				tempInstance.setInstanceState(instanceState);
				tempInstance.setProviderName("OPENSTACK");
				openstackInstanceMap.put(instanceId, tempInstance);
				middlewareInstanceHash.put(instanceId, tempInstance);
				//System.out.println(tempServers.get("id") + ":" + tempServers.get("name")+":"+IPAddress);
				middlewareId = tempInstance.getInstanceId();
				
				
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public OpenstackDriver(String inputDetails) throws Exception, IOException,
	InterruptedException {
		Object obj = JSONValue.parse(inputDetails);
		JSONObject jsonObject = (JSONObject) obj;
		userName = (String) jsonObject.get("username");
		password = (String) jsonObject.get("password");
		authUrl = (String) jsonObject.get("authurl");
		//computeUrl = (String) jsonObject.get("computeurl");
		computeUrl = authUrl+":8774/v2";
		String jsonRestInput;
		jsonRestInput = "{\"auth\": {\"passwordCredentials\": {\"username\": \""
				+ userName
				+ "\", \"password\": \""
				+ password
				+ "\"}, \"tenantName\": \"" + userName + "\"}}";

		/*
		 * Using RESTFUL API to get the authentication token Id (1st step,
		 * Authentication Service) before executing any OpenStack compute
		 * operations
		 */

		HttpClient c = new DefaultHttpClient();
		HttpPost p = new HttpPost(authUrl+":5000/v2.0/tokens");
		p.setHeader("Content-Type", "application/json");
		p.setHeader("Accept", "application/json");
		p.setHeader("User-Agent", "python-novaclient");

		p.setEntity(new StringEntity((jsonRestInput), "utf-8"));
		HttpResponse r = c.execute(p);
		BufferedReader rd = new BufferedReader(new InputStreamReader(r
				.getEntity().getContent()));
		String line = "";
		while ((line = rd.readLine()) != null) {
			//System.out.println(line);
			Object jsonResponseLine = JSONValue.parse(line);
			JSONObject jsonResponseObject = (JSONObject) jsonResponseLine;
			JSONObject tempAccess = (JSONObject) jsonResponseObject
					.get("access");
			JSONObject tempToken = (JSONObject) tempAccess.get("token");
			JSONObject tempTenant = (JSONObject) tempToken.get("tenant");
			tenantId = (String) tempTenant.get("id");
			authenticationToken = (String) tempToken.get("id");
		}
		setExistingInstances();

	} // End of Constructor


	@Override
	public Instance createInstance(String args) {
		System.out.println("Launching an instance from the image...");
		// TODO Auto-generated method stub
		Object obj = JSONValue.parse(args);
		JSONObject jsonObject = (JSONObject) obj;
		
		String imageId = (String) jsonObject.get("imageid");
		String instanceName = imageId+"_OS_instance";
		String flavorId = (String) jsonObject.get("flavorid");
		String keyname = "None";//(String) jsonObject.get("keyname");
		int minCount = Integer.valueOf((String) jsonObject.get("mincount"));
		int maxCount = Integer.valueOf((String) jsonObject.get("maxcount"));
		String instanceId = "";
		String instanceIP = "";

		try {

			/*
			 * Instance Creation Steps
			 */

			String instanceUrl = authUrl+":8774/v2" + "/" + tenantId + "/servers";
			String jsonInstanceCreation = "{\"server\": {\"min_count\": "
					+ minCount 
					+ ",  \"flavorRef\": \"" + flavorId + "\", \"name\": \""
					+ instanceName + "\", \"imageRef\": \"" + imageId
					+ "\", \"max_count\": " + maxCount
					+ ", \"security_groups\": [{\"name\": \"default\"}]}}";
			//System.out.println(jsonInstanceCreation);
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(instanceUrl);
			httpPost.setHeader("X-Auth-Project-Id", userName);
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("User-Agent", "python-novaclient");
			httpPost.setHeader("Content-Type", "application/json");
			httpPost.setHeader("X-Auth-Token", authenticationToken);
			httpPost.setEntity(new StringEntity((jsonInstanceCreation), "utf-8"));
			HttpResponse httpResponse = httpClient.execute(httpPost);
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					httpResponse.getEntity().getContent()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				System.out.println(line);
				Object jsonResponseLine = JSONValue.parse(line);
				JSONObject jsonResponseObject = (JSONObject) jsonResponseLine;
				JSONObject tempServer = (JSONObject) jsonResponseObject
						.get("server");
				instanceId = (String) tempServer.get("id");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		OpenstackInstance ourInstance = new OpenstackInstance();
		ourInstance.setInstanceName(instanceName);
		ourInstance.setImageId(imageId);
		ourInstance.setInstanceId(instanceId);
		ourInstance.setInstanceState(WaitforInstanceActivation(instanceId));
		ourInstance.setInstanceIP(getInstanceIP(instanceId));
		ourInstance.setProviderName("OPENSTACK");
		openstackInstanceMap.put(ourInstance.getInstanceId(), ourInstance);
		middlewareInstanceHash.put(ourInstance.getInstanceId(), ourInstance);
		return ourInstance;
	}

	@Override
	public void describeAllInstances() {

		/*
		 * Instance Creation Steps
		 */
		for(String id: middlewareInstanceHash.keySet()) {
			System.out.println(middlewareInstanceHash.get(id));
		}
	}

	@Override
	public void listAllImages() {
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void createKeyPair(String name) {
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void listAllVolumes() {
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void pauseInstance(Instance ourInstance) {
		// TODO Auto-generated method stub

		try {

			/*
			 * Instance Pause Steps
			 */

			String instanceId = "d7532a41-de1d-4773-ab71-7b95b2aa656d";
			String instanceUrl = computeUrl + "/" + tenantId + "/servers/"
					+ instanceId + "/action";
			String jsonBody = "{\"os-stop\": null}";
			// String jsonInstanceCreation =
			// "{\"server\": {\"min_count\": "+minCount+", \"flavorRef\": \""+flavorId+"\", \"name\": \""+instanceName+"\", \"imageRef\": \""+imageId+"\", \"max_count\": "+maxCount+"}}";
			// String jsonInstanceCreation =
			// "{\"server\": {\"min_count\": "+minCount+", \"key_name\":  \""+keyname+"\",  \"flavorRef\": \""+flavorId+"\", \"name\": \""+instanceName+"\", \"imageRef\": \""+imageId+"\", \"max_count\": "+maxCount+", \"security_groups\": [{\"name\": \"default\"}]}}";
			// System.out.println(jsonInstanceCreation);
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(instanceUrl);
			httpPost.setHeader("X-Auth-Project-Id", userName);
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("User-Agent", "python-novaclient");
			httpPost.setHeader("Content-Type", "application/json");
			httpPost.setHeader("X-Auth-Token", authenticationToken);
			httpPost.setEntity(new StringEntity((jsonBody), "utf-8"));
			HttpResponse httpResponse = httpClient.execute(httpPost);
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					httpResponse.getEntity().getContent()));
			String line = "";

			while ((line = rd.readLine()) != null) {
				System.out.println(line);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void resumeInstance(Instance ourInstance) {
		// TODO Auto-generated method stub

		try {

			/*
			 * Instance Pause Steps
			 */

			String instanceId = "d7532a41-de1d-4773-ab71-7b95b2aa656d";
			String instanceUrl = computeUrl + "/" + tenantId + "/servers/"
					+ instanceId + "/action";
			String jsonBody = "{\"os-start\": null}";
			// String jsonInstanceCreation =
			// "{\"server\": {\"min_count\": "+minCount+", \"flavorRef\": \""+flavorId+"\", \"name\": \""+instanceName+"\", \"imageRef\": \""+imageId+"\", \"max_count\": "+maxCount+"}}";
			// String jsonInstanceCreation =
			// "{\"server\": {\"min_count\": "+minCount+", \"key_name\":  \""+keyname+"\",  \"flavorRef\": \""+flavorId+"\", \"name\": \""+instanceName+"\", \"imageRef\": \""+imageId+"\", \"max_count\": "+maxCount+", \"security_groups\": [{\"name\": \"default\"}]}}";
			// System.out.println(jsonInstanceCreation);
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(instanceUrl);
			httpPost.setHeader("X-Auth-Project-Id", userName);
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("User-Agent", "python-novaclient");
			httpPost.setHeader("Content-Type", "application/json");
			httpPost.setHeader("X-Auth-Token", authenticationToken);
			httpPost.setEntity(new StringEntity((jsonBody), "utf-8"));
			HttpResponse httpResponse = httpClient.execute(httpPost);
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					httpResponse.getEntity().getContent()));
			String line = "";

			while ((line = rd.readLine()) != null) {
				System.out.println(line);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void terminateInstance(Instance ourInstance) {
		// TODO Auto-generated method stub
		try {

			/*
			 * Instance Pause Steps
			 */

			String instanceId = "0b30bd66-694a-4142-8566-a46de5f09b94";
			String instanceUrl = computeUrl + "/" + tenantId + "/servers/"
					+ instanceId;
			// String jsonBody = "{\"os-start\": null}";
			// String jsonInstanceCreation =
			// "{\"server\": {\"min_count\": "+minCount+", \"flavorRef\": \""+flavorId+"\", \"name\": \""+instanceName+"\", \"imageRef\": \""+imageId+"\", \"max_count\": "+maxCount+"}}";
			// String jsonInstanceCreation =
			// "{\"server\": {\"min_count\": "+minCount+", \"key_name\":  \""+keyname+"\",  \"flavorRef\": \""+flavorId+"\", \"name\": \""+instanceName+"\", \"imageRef\": \""+imageId+"\", \"max_count\": "+maxCount+", \"security_groups\": [{\"name\": \"default\"}]}}";
			// System.out.println(jsonInstanceCreation);
			HttpClient httpClient = new DefaultHttpClient();
			HttpDelete httpDelete = new HttpDelete(instanceUrl);
			httpDelete.setHeader("X-Auth-Project-Id", userName);
			httpDelete.setHeader("Accept", "application/json");
			httpDelete.setHeader("User-Agent", "python-novaclient");
			httpDelete.setHeader("Content-Type", "application/json");
			httpDelete.setHeader("X-Auth-Token", authenticationToken);
			// httpDelete.setEntity(new StringEntity((jsonBody),"utf-8"));
			HttpResponse httpResponse = httpClient.execute(httpDelete);
			/*
			 * BufferedReader rd = new BufferedReader(new
			 * InputStreamReader(httpResponse.getEntity().getContent())); String
			 * line = "";
			 * 
			 * while((line = rd.readLine())!=null) { System.out.println(line);
			 * 
			 * }
			 */
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getInstanceState(Instance ourInstance) {
		// TODO Auto-generated method stub
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Volume createVolume(String args) {
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteVolume(Volume ourVolume) {
		// TODO Auto-generated method stub
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void attachVolume(Volume ourVolume, Instance ourInstance,
			String deviceName) {
		// TODO Auto-generated method stub
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void detachVolume(Volume ourVolume) {
		// TODO Auto-generated method stub
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getVolumeState(Volume ourVolume) {
		// TODO Auto-generated method stub
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String getAvailabilityZone(Instance ourInstance) {
		// TODO Auto-generated method stub
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String deployApplication(String inputDetails, String instanceId) {
		// TODO Auto-generated method stub
		DeployApp scp = new DeployApp(inputDetails, instanceId);
		scp.scp();
		return null;
	}

	@Override
	public Instance getInstance(String instanceId) {
		// TODO Auto-generated method stub
		
		return middlewareInstanceHash.get(instanceId);
	}

	@Override
	public Instance importInstance(String input) {
		// TODO Auto-generated method stub



		// TODO Auto-generated method stub

		
		FlavorMap f = new FlavorMap();
		String instanceCreateOpenstack = "";
		OpenstackInstance openstackInstance = new OpenstackInstance();
		String imageId = "";
		Object obj = JSONValue.parse(input);
		JSONObject jsonObject = (JSONObject) obj;
		String mode = (String) jsonObject.get("mode");
		if(mode.equalsIgnoreCase("remote"))
		{
			System.out.println("Creating Instance from the Image ......");
			return createInstance(input);
		}
		String imageName = (String) jsonObject.get("name");
		String diskFormat = (String) jsonObject.get("format");
		String minDisk = (String) jsonObject.get("mindisk");
		String minRam = (String) jsonObject.get("minram");
		String Architecture = (String) jsonObject.get("architecture");
		String filePath = (String) jsonObject.get("path");
		
		String instanceId = "";
		System.out.println("Uploading Image to Openstack .....");
		try {

			/*
			 * Instance Creation Steps
			 */

			/**
			 * imageUrl to be changed properly

			 */
			File file = new File(filePath);

			FileEntity fileEntity = new FileEntity(file,"application/octet-stream");
			String size = String.valueOf(file.length());

			String imageUrl =authUrl+":9292/v1/images";
			//"http://192.168.0.100:9292/v1/images";
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(imageUrl);

			
			httpPost.setHeader("x-image-meta-container_format", "bare");
			httpPost.setHeader("x-image-meta-min_disk", minDisk);
			httpPost.setHeader("User-Agent", "python-glanceclient");
			httpPost.setHeader("x-image-meta-size", size);
			httpPost.setHeader("x-image-meta-property-architecture", Architecture);
			httpPost.setHeader("x-image-meta-is_public", "True");
			httpPost.setHeader("x-image-meta-min_ram", minRam);
			httpPost.setHeader("Content-Type", "application/octet-stream");
			httpPost.setHeader("x-image-meta-disk_format", diskFormat.toLowerCase());
			httpPost.setHeader("x-image-meta-name", imageName);
			httpPost.setHeader("X-Auth-Token", authenticationToken);



			httpPost.setEntity(fileEntity);


			HttpResponse httpResponse = httpClient.execute(httpPost);
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					httpResponse.getEntity().getContent()));

			String line = "";

			while ((line = rd.readLine()) != null) {
				//System.out.println(line);
				obj = JSONValue.parse(line);
				jsonObject = (JSONObject) obj;
				JSONObject imageDetails = (JSONObject) jsonObject.get("image");
				imageId = (String) imageDetails.get("id");
			}

			//System.out.println(imageId);

			/*Instance is being created with the uploaded image*/
			
			String flavor = f.openstackMap(input);
			System.out.println("Flavor is: "+flavor);
			 instanceCreateOpenstack = 
					"{" 
							+ "\"imageid\": \""+imageId+"\","
							+ "\"flavorid\": \""+flavor+"\","
							+ "\"mincount\": \"1\","
							+ "\"maxcount\": \"1\","
							+ "\"computeurl\": \"http://192.168.0.100:8774/v2\""
							+ "}"; 

			//createInstance(instanceCreateOpenstack);
		} catch (Exception e) {
			e.printStackTrace();
		}

  System.out.println("Successfully uploaded the image");

		
	
		return createInstance(instanceCreateOpenstack);

	}

	

	@Override
	public String exportInstance(String args)  {
		try {
			System.out.println("Extracting SNAP ID");
			Object obj = JSONValue.parse(args);
			JSONObject jsonObject = (JSONObject) obj;

			String instanceId = (String) jsonObject.get("instanceid");
			String imageName = (String) jsonObject.get("imagename");

			process("nova  --os-username " + userName + " --os-password " + password
					+ " --os-tenant-name " + userName
					+ " --os-auth-url "+authUrl+":5000/v2.0 image-create "+instanceId +" "+imageName);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	public void process(String cmd) throws IOException, InterruptedException {
		String s;

		Process p = Runtime.getRuntime().exec(cmd);
		BufferedReader br = new BufferedReader(new InputStreamReader(
				p.getInputStream()));

		while ((s = br.readLine()) != null){
			System.out.println("output: " + s);
		}
		p.waitFor();
	//	System.out.println("exit: " + p.exitValue());
		p.destroy();

	}
	public String extractSnapID(String snapshotName) {
		String s = "";
		try {
			
			System.out.println("Extracting SNAP ID");

			String cmd[] = { "/bin/sh",
					"-c",
					"nova --os-username " + userName + " --os-password " + password
					+ " --os-tenant-name " + userName
					+ " --os-auth-url "+authUrl + ":5000/v2.0 image-list | grep '"+snapshotName+"' | awk '{print$2}'"};

			Process p = Runtime.getRuntime().exec(cmd);

			BufferedReader br = new BufferedReader(new InputStreamReader(
					p.getInputStream()));
			String temp = "";
			while ((temp = br.readLine()) != null){
				s = temp;
				System.out.println("Snapshot Id : " + s);

			}
			p.waitFor();
		//	System.out.println("exit: " + p.exitValue());
			p.destroy();
		} catch (Exception e) {
		}
		return s;
	}




	public void downloadRAW(String rawName, String SnapshotID) {
		try {
			System.out.println("Downloading RAW file");

			process("glance --os-username " + userName + " --os-password " + password
					+ " --os-tenant-name " + userName
					+ " --os-auth-url "+authUrl +
					":5000/v2.0 image-download --file "+rawName+" "+SnapshotID);

		} catch (Exception e) {
		}

	}

	public void convertToVMDK(String Source, String Destination) {
		try {
			System.out.println("converting RAW to VMDK ");

			process("qemu-img convert "+Source+" -O vmdk "+Destination);

		} catch (Exception e) {
		}

	}




	public void convertToVHD(String Source, String Destination) {
		try {			

			System.out.println("converting VMDK to VHD ");
			process("VBoxManage clonehd "+Source+" "+Destination+" --format VHD");

		} catch (Exception e) {
		}
		
	}

	public void waitforSnapShotCompletion(String imagename) {
		try {		

			System.out.println("Waiting for SnapShot Completion");
			String s = "null";
			while( !s.equals("ACTIVE")){

				String cmd[] = { "/bin/sh",
						"-c",
						"nova --os-username " + userName + " --os-password " + password
						+ " --os-tenant-name " + userName
						+ " --os-auth-url "+authUrl + ":5000/v2.0 image-list | grep '"+imagename+"' | awk '{print$6}'"};

				Process p = Runtime.getRuntime().exec(cmd);
				BufferedReader br = new BufferedReader(new InputStreamReader(
						p.getInputStream()));
				String tempStr = "";
				while ((tempStr = br.readLine()) != null){
					s = tempStr;
					System.out.println("status: " + s+"... ");
					Thread.sleep(7000);

				}
				p.waitFor();
				//System.out.println("exit: " + p.exitValue());
				p.destroy();
				
			}	

		} catch (Exception e) {
		}

		// TODO Auto-generated method stub

	}
	public String WaitforInstanceActivation(String instanceId)
	{
		String s = "null";
		try {		

			System.out.println(" Waiting to Start the Instance ......");
			
			while( !s.equals("ACTIVE")){

				String cmd[] = { "/bin/sh",
						"-c",
						"nova --os-username " + userName + " --os-password " + password
						+ " --os-tenant-name " + userName
						+ " --os-auth-url "+authUrl + ":5000/v2.0 list | grep '"+instanceId+"' | awk '{print$6}'"};

				Process p = Runtime.getRuntime().exec(cmd);
				BufferedReader br = new BufferedReader(new InputStreamReader(
						p.getInputStream()));
				String tempStr = "";
				while ((tempStr = br.readLine()) != null){
					s = tempStr;
					//System.out.println("line: " + s);

				}
				p.waitFor();
				//System.out.println("exit: " + p.exitValue());
				p.destroy();
				
			}	

		} catch (Exception e) {
		}

		// TODO Auto-generated method stub

	
		System.out.println("Instance is: "+s);
		return s;
	}

	public String getCredentials() {
		// TODO Auto-generated method stub
		String openstackCredentials = 
				"{"
				+ "\"provider\" : \"Openstack\","
				+ "\"username\" : \""+ userName+"\","
				+ "\"password\" : \""+ password +"\""
				+ "\"authurl\" : \""+ authUrl +"\""
				+ "\"computeurl\" : \""+ computeUrl +"\""
				+ "}"; 
		return openstackCredentials;
	}

	public String getInstanceIP(String instanceId)
	{
		

		String s = "null";
		try {		
				
			String cmd[] = { "/bin/sh",
					"-c",
					"nova --os-username " + userName + " --os-password " + password
					+ " --os-tenant-name " + userName
					+ " --os-auth-url "+authUrl + ":5000/v2.0 list | grep '"+instanceId+"' | awk '{print$12}'"};

			Process p = Runtime.getRuntime().exec(cmd);
			BufferedReader br = new BufferedReader(new InputStreamReader(
					p.getInputStream()));
			String tempStr = "";
			while ((tempStr = br.readLine()) != null){
				s = tempStr;
				
			}
			p.waitFor();
			System.out.println("exit: " + p.exitValue());
			p.destroy();
			}	

		 catch (Exception e) {
		}

		// TODO Auto-generated method stub

		String[] split = s.split("=");
		
		return split[1];
	
	}

	@Override
	public boolean resizeInstance(Instance instance, String flavorDetails) {
		// TODO Auto-generated method stub
		try
		{
		FlavorMap f = new FlavorMap();
		String flavor = f.openstackMap(flavorDetails);
		process("nova --os-username " + userName + " --os-password " + password
					+ " --os-tenant-name " + userName
					+ " --os-auth-url "+authUrl + ":5000/v2.0 resize "+instance.getInstanceId()+" "+flavor+" --poll");
		System.out.println("Resize Done.... waiting for confirmation");
		process("nova --os-username " + userName + " --os-password " + password
					+ " --os-tenant-name " + userName
					+ " --os-auth-url "+authUrl + ":5000/v2.0 resize-confirm "+instance.getInstanceId());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public String createSecurityGroup(String input) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void associateSecurityGroupWithInstance(String securityGroupName,
			Instance instanceName) {
		// TODO Auto-generated method stub
		
	}


}