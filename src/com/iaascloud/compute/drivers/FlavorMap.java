package com.iaascloud.compute.drivers;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class FlavorMap {

/*	public static void main(String[] args) {
		FlavorMap f = new FlavorMap();
		String userFlavorDetails = "{"
				+ "\"name\": \"Ubuntu_OS_ex\","
				+ "\"diskformat\": \"vhd\","
				+ "\"mindisk\": \"8\","
				+ "\"minram\": \"10000\","
				+ "\"architecture\": \"x86_64\","
				+ "\"filepath\": \"/opt/stack/Documents/Ubuntu_AWS/Ubuntu_AWS.vhd\""
				+ "}";
		
		String amazonFlavour = f.amazonFlavorMap(userFlavorDetails);		
		String openStackFlavour = f.openstackMap(userFlavorDetails);
		System.out.println("Amazon "+amazonFlavour+" OpenStack "+openStackFlavour);
	}*/

	public String amazonFlavorMap(String userFlavorDetails) {

		

		String AmazonMap = "{ \"flavors\" : ["
				+ "{\"name\" : \"m3.medium\", \"minram\" : \"3.75\",\"mindisk\" : \"4\"},"
				+ "{\"name\" : \"m3.large\", \"minram\" : \"7.5\",\"mindisk\" : \"32\"},"
				+ "{\"name\" : \"m3.xlarge\", \"minram\" : \"15\",\"mindisk\" : \"80\"},"
				+ "{\"name\" : \"m3.2xlarge\", \"minram\" : \"30\",\"mindisk\" : \"160\"},"
				+ "{\"name\" : \"m1.small\", \"minram\" : \"1.7\",\"mindisk\" : \"160\"}"

				+ " ]" + "}";

		Object userObj = JSONValue.parse(userFlavorDetails);

		JSONObject userJsonObject = (JSONObject) userObj;
		double userMinRamMB = Double.parseDouble((String) userJsonObject
				.get("minram"));
		double userMinRam = (double) (userMinRamMB / 1024);
		Double userMinDisk = Double.parseDouble((String) userJsonObject
				.get("mindisk"));
		//System.out.println("userMinRam: " + userMinRam + " userMinDisk: "
		//		+ userMinDisk);

		Object amazonObj = JSONValue.parse(AmazonMap);

		JSONObject amazonJsonObject = (JSONObject) amazonObj;
		JSONArray flavorArray = (JSONArray) amazonJsonObject.get("flavors");
		double minRamDiff = -1;
		String minRamIndex = "";
		for (int i = 0; i < flavorArray.size(); i++) {

			JSONObject flavorDetails = (JSONObject) flavorArray.get(i);
			double amazonMinRam = Double.parseDouble((String) flavorDetails
					.get("minram"));
			double amazonMinDisk = Double.parseDouble((String) flavorDetails
					.get("mindisk"));
			String name = (String) flavorDetails.get("name");
			//System.out.println(name + " " + amazonMinRam + " " + amazonMinDisk);

			if (userMinRam <= amazonMinRam) {
				double ramDiff = amazonMinRam - userMinRam;

				// When the user RAM specification matches for the first time
				// assign the current RAM values and flavor name.
				if (minRamDiff == -1) {
					minRamDiff = ramDiff;
					minRamIndex = name;
				}

				// Assign flavor which is closest to the user RAM specification
				if (minRamDiff > ramDiff) {

					minRamDiff = ramDiff;
					minRamIndex = name;

				}
			}

		}// end of minRam index

		if (minRamIndex.length() == 0)
			return("User should give a bigger smaller RAM size");
		else
			return(minRamIndex);

	}
	
	public String openstackMap(String userFlavorDetails)
	{


		String openstackMap = "{ \"flavors\" : ["
				+ "{\"name\" : \"3\", \"minram\" : \"4\",\"mindisk\" : \"40\"},"
				+ "{\"name\" : \"4\", \"minram\" : \"8\",\"mindisk\" : \"80\"},"
				+ "{\"name\" : \"5\", \"minram\" : \"16\",\"mindisk\" : \"160\"},"
				+ "{\"name\" : \"2\", \"minram\" : \"2\",\"mindisk\" : \"20\"}"
				+ " ]" + "}";

		Object userObj = JSONValue.parse(userFlavorDetails);

		JSONObject userJsonObject = (JSONObject) userObj;
		double userMinRamMB = Double.parseDouble((String) userJsonObject
				.get("minram"));
		double userMinRam = (double) (userMinRamMB / 1024);
		Double userMinDisk = Double.parseDouble((String) userJsonObject
				.get("mindisk"));
		//System.out.println("userMinRam: " + userMinRam + " userMinDisk: "
		//		+ userMinDisk);

		Object openstackObj = JSONValue.parse(openstackMap);

		JSONObject openstackJsonObject = (JSONObject) openstackObj;
		JSONArray flavorArray = (JSONArray) openstackJsonObject.get("flavors");
		double minRamDiff = -1;
		String minRamIndex = "";
		for (int i = 0; i < flavorArray.size(); i++) {

			JSONObject flavorDetails = (JSONObject) flavorArray.get(i);
			double openstackMinRam = Double.parseDouble((String) flavorDetails
					.get("minram"));
			double openstackMinDisk = Double.parseDouble((String) flavorDetails
					.get("mindisk"));
			String name = (String) flavorDetails.get("name");
			//System.out.println(name + " " + amazonMinRam + " " + amazonMinDisk);

			if (userMinRam <= openstackMinRam) {
				double ramDiff = openstackMinRam - userMinRam;

				// When the user RAM specification matches for the first time
				// assign the current RAM values and flavor name.
				if (minRamDiff == -1) {
					minRamDiff = ramDiff;
					minRamIndex = name;
				}

				// Assign flavor which is closest to the user RAM specification
				if (minRamDiff > ramDiff) {

					minRamDiff = ramDiff;
					minRamIndex = name;

				}
			}

		}// end of minRam index

		if (minRamIndex.length() == 0)
			return("User should give a bigger  RAM size");
		else
			return(minRamIndex);

	}

}
