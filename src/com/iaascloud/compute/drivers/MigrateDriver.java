package com.iaascloud.compute.drivers;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.iaascloud.compute.Providers;
import com.iaascloud.compute.instances.Instance;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

public class MigrateDriver {
	private  Providers sourceDriver;
	private  Providers destinationDriver;
	private	 Instance sourceInstance;
	private  Instance instance;
	private  ProviderDriver amazonDriver;
	private  ProviderDriver openstackDriver; 
	private String providerName;
	
	private String workspacePath;
	
	public MigrateDriver(ProviderDriver srcDriver,  ProviderDriver destDriver, Instance sourceInstanceId) {
		
		this.amazonDriver = srcDriver;
		this.openstackDriver = destDriver;
		this.sourceInstance = sourceInstanceId;
		workspacePath = "/opt/stack";
		
		/*if(AmazonEC2Driver.ec2InstanceMap.containsKey(sourceInstanceId))
		{
			this.sourceDriver = Providers.AMAZON;
			this.destinationDriver = Providers.OPENSTACK;
			
		}
		else if(OpenstackDriver.openstackInstanceMap.containsKey(sourceInstanceId))
		{
			this.sourceDriver = Providers.OPENSTACK;
			this.destinationDriver = Providers.AMAZON;
		}*/
		
		if(sourceInstanceId.getProviderName().equals("AMAZON"))
		{
			this.sourceDriver = Providers.AMAZON;
			this.destinationDriver = Providers.OPENSTACK;
		}
		else
		{
			this.sourceDriver = Providers.OPENSTACK;
			this.destinationDriver = Providers.AMAZON;
		}
	}
	
	void setWorkspacePath(String path) {
		this.workspacePath = path;
	}
	
	private static void displayTextInputStream(InputStream input)
		    throws IOException {
		    	// Read one text line at a time and display.
		        BufferedReader reader = new BufferedReader(new 
		        		InputStreamReader(input));
		        while (true) {
		            String line = reader.readLine();
		            if (line == null) break;

		            System.out.println("    " + line);
		        }
		        System.out.println();
		    }
	
	public Instance migrate() throws IOException {
		Instance migratedInstance =  null; 
		System.out.println("Source: "+this.sourceDriver+" Dest: "+this.destinationDriver);
		if(sourceDriver == Providers.AMAZON && destinationDriver == Providers.OPENSTACK) {
			/*
			 * Migrate from Amazon to Openstack
			 */
			
			AmazonEC2Driver ec2Driver = (AmazonEC2Driver) amazonDriver;
			String exportResult = ec2Driver.exportInstance(sourceInstance.getInstanceId());
			
			Object obj = JSONValue.parse(exportResult);
			JSONObject jsonObject = (JSONObject)obj;
			String bucketName = (String) jsonObject.get("bucketName");
			String S3Key = (String) jsonObject.get("S3Key");
			
			//String bucketName = "iaasmiddleware";
			//String S3Key = "export-i-fg8vhyi5.vhd";
			AmazonS3 s3Client = new AmazonS3Client(ec2Driver.getCredentials());
			GetObjectRequest request = new GetObjectRequest(bucketName, S3Key);
			S3Object object = s3Client.getObject(request);
			S3ObjectInputStream objectContent = object.getObjectContent();
			System.out.println("Export from Amazon Successful. Downloading to local file system...");
			IOUtils.copy(objectContent, new FileOutputStream(workspacePath + "/" + S3Key));
			System.out.println("Download from Amazon Completed...");
			
			
			String localInstancePath = workspacePath + "/" + S3Key;
			String inputToOpenstack = 
					"{" 
					+ "\"name\": \"Ubuntu_OS_ex\","
					+ "\"format\": \"vhd\","
					+ "\"mindisk\": \"8\","
					+ "\"minram\": \"1024\","
					+ "\"mode\": \"local\","
					+ "\"architecture\": \"x86_64\","
					+ "\"path\": \""+localInstancePath+"\""
					+ "}";
			
			object.close();
			migratedInstance = openstackDriver.importInstance(inputToOpenstack);
			
		} // End of conversion of Amazon to Openstack
		else if(sourceDriver == Providers.OPENSTACK && destinationDriver == Providers.AMAZON) {
			/*
			 * Migrate from Openstack to Amazon
			 */
			
			String imageName = sourceInstance.getInstanceId() + "_Snapshot";
			
			String exportInstance =
					"{" 
							+ "\"instanceid\": \""+sourceInstance.getInstanceId() +"\","
							+ "\"imagename\": \""+imageName+"\","
							+ "}";
			
			String SourceRAW = sourceInstance.getInstanceId() + "_RAW.raw";
			String vmdkFileName = sourceInstance.getInstanceId() + "_VMDK.vmdk";
			String vhdFileName =sourceInstance.getInstanceId() + "_VHD.vhd";
			
			
			openstackDriver.exportInstance(exportInstance);
			
			OpenstackDriver  o = (OpenstackDriver) openstackDriver;
			
			//Wait until completion
			o.waitforSnapShotCompletion(imageName);
			
			//Take ID of the SNAP SHOT
			
			String snapID= o.extractSnapID(imageName);
			o.downloadRAW(SourceRAW, snapID);
			o.convertToVMDK(SourceRAW, vmdkFileName);
			o.convertToVHD(vmdkFileName, vhdFileName);
			
			System.out.println("Exported Instance from Openstack Successfully... Importing it to Amazon...");
			String vhdPath = "/opt/stack/projecta/iaas-cloud-middleware/"+vhdFileName;
			String importInstanceJSON = 
					"{"
					+ "\"mode\" : \"local\""
					+ "\"bucket\" : \"iaasmiddleware\""
					+ "\"path\" : \""+vhdPath+"\""
					+ "\"architecture\" : \"x86_64\""
					+ "\"mindisk\": \"8\","
					+ "\"minram\": \"2048\","
					+ "\"format\" : \"VHD\""
					+ "\"platform\" : \"Linux\""
					+ "\"region\" : \"us-west-2\""
					+ "}";
			
			migratedInstance = amazonDriver.importInstance(importInstanceJSON);
		}
		return migratedInstance;
 
	}
}
