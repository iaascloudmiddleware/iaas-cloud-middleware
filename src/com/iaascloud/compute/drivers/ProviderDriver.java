package com.iaascloud.compute.drivers;

import java.util.ArrayList;
import java.util.HashMap;

import com.iaascloud.compute.instances.Instance;
import com.iaascloud.compute.volumes.Volume;

public interface ProviderDriver { 
	public static HashMap<String,Instance> middlewareInstanceHash = new HashMap<String,Instance>();
	public Instance createInstance(String args);
	public void describeAllInstances(); 
	public void listAllImages(); 
	public void createKeyPair(String name); 
	public void listAllVolumes();
	public void pauseInstance(Instance ourInstance); 
	public void resumeInstance(Instance ourInstance);
	public void terminateInstance(Instance ourInstance);
	public String getInstanceState(Instance ourInstance);
	public String getVolumeState(Volume ourVolume);
	public Volume createVolume(String args);
	public void deleteVolume(Volume ourVolume); 
	public void attachVolume(Volume ourVolume, Instance ourInstance, String deviceName);
	public void detachVolume(Volume ourVolume);
	public String getAvailabilityZone(Instance ourInstance);
	public String deployApplication(String inputDetails, String instanceId);
	public Instance getInstance(String instanceId);
	public Instance importInstance(String input);
	public String exportInstance(String input);
	public boolean resizeInstance(Instance instance,String flavorDetails);
	public String createSecurityGroup(String input);
	public void associateSecurityGroupWithInstance(String securityGroupName, Instance instanceName);
}
