package com.iaascloud.compute.drivers;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.AttachVolumeRequest;
import com.amazonaws.services.ec2.model.AuthorizeSecurityGroupIngressRequest;
import com.amazonaws.services.ec2.model.CreateInstanceExportTaskRequest;
import com.amazonaws.services.ec2.model.CreateInstanceExportTaskResult;
import com.amazonaws.services.ec2.model.CreateKeyPairRequest;
import com.amazonaws.services.ec2.model.CreateSecurityGroupRequest;
import com.amazonaws.services.ec2.model.CreateSecurityGroupResult;
import com.amazonaws.services.ec2.model.CreateVolumeRequest;
import com.amazonaws.services.ec2.model.CreateVolumeResult;
import com.amazonaws.services.ec2.model.DeleteVolumeRequest;
import com.amazonaws.services.ec2.model.DescribeConversionTasksResult;
import com.amazonaws.services.ec2.model.DescribeExportTasksRequest;
import com.amazonaws.services.ec2.model.DescribeExportTasksResult;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.DetachVolumeRequest;
import com.amazonaws.services.ec2.model.DiskImage;
import com.amazonaws.services.ec2.model.DiskImageDetail;
import com.amazonaws.services.ec2.model.ExportToS3TaskSpecification;
import com.amazonaws.services.ec2.model.ImportInstanceLaunchSpecification;
import com.amazonaws.services.ec2.model.ImportInstanceRequest;
import com.amazonaws.services.ec2.model.InstanceAttributeName;
import com.amazonaws.services.ec2.model.IpPermission;
import com.amazonaws.services.ec2.model.ModifyInstanceAttributeRequest;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.StartInstancesRequest;
import com.amazonaws.services.ec2.model.StopInstancesRequest;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;
import com.amazonaws.services.ec2.model.VolumeDetail;
import com.iaascloud.compute.instances.AmazonEC2Instance;
import com.iaascloud.compute.instances.Instance;
import com.iaascloud.compute.volumes.AmazonEC2Volume;
import com.iaascloud.compute.volumes.Volume;
import com.iaascloud.deploy.DeployApp;



public class AmazonEC2Driver implements ProviderDriver {
	AWSCredentials credentials;
	int count = 0;
	
	String middlewareId = "";
	AmazonEC2Client ec2, ec2_east;
	public static HashMap<String, Instance> ec2InstanceMap = new HashMap<String, Instance>();
	int TIMEOUTPERIOD = 50;
	String endPoints = 
			"{\"us-west-2\": {"
			+ "\"endpoint\": \"ec2.us-west-2.amazonaws.com\","
			+ "\"api_name\": \"ec2_us_west_oregon\"," + "\"country\": \"US\""
			+ "}," 
			+ "\"us-west-1\": {"
			+ "\"endpoint\": \"ec2.us-west-1.amazonaws.com\","
			+ "\"api_name\": \"ec2_us_west\"," + "\"country\": \"USA\"" + "}"
			+ "}";

	private void setExistingInstances(DescribeInstancesResult input) {
		for (int i = 0; i < input.getReservations().size(); i++) {
			for (int j = 0; j < input.getReservations().get(i).getInstances()
					.size(); j++) {
				AmazonEC2Instance tempInstance = new AmazonEC2Instance();
				tempInstance.setInstanceId(input.getReservations().get(i)
						.getInstances().get(j).getInstanceId());
				tempInstance.setImageId(input.getReservations().get(i)
						.getInstances().get(j).getImageId());
				tempInstance.setInstanceName("");
				tempInstance.setInstanceState(input.getReservations().get(i)
						.getInstances().get(j).getState().getName());
				tempInstance.setInstanceIP(input.getReservations().get(i)
						.getInstances().get(j).getPublicIpAddress());
				tempInstance.setProviderName("AMAZON");
				ec2InstanceMap.put(tempInstance.getInstanceId(), tempInstance);
						
				middlewareInstanceHash.put(tempInstance.getInstanceId(),tempInstance);
			}
		}
	}

	public AmazonEC2Driver(String inputDetails) throws IOException {
		Object obj = JSONValue.parse(inputDetails);
		JSONObject jsonObject = (JSONObject) obj;
		String keypath = (String) jsonObject.get("keypath");
		String endpoint = (String) jsonObject.get("endpoint");

		obj = JSONValue.parse(endPoints);
		jsonObject = (JSONObject) obj;
		JSONObject object1 = (JSONObject) jsonObject.get(endpoint);

		endpoint = (String) object1.get("endpoint");

		credentials = new PropertiesCredentials(new File(keypath));
		ec2 = new AmazonEC2Client(credentials);
		ec2_east = new AmazonEC2Client(credentials);

		ec2.setEndpoint(endpoint);
		ec2_east.setEndpoint("ec2.us-east-1.amazonaws.com");
		setExistingInstances(ec2.describeInstances());
	}


	@Override
	public Instance createInstance(String args) {
		// TODO Exception Handling
		System.out.println("Success");
		Object obj = JSONValue.parse(args);
		JSONObject jsonObject = (JSONObject) obj;
		String image = (String) jsonObject.get("image");
		String instancetype = (String) jsonObject.get("instancetype");
		int mincount = Integer.parseInt((String) jsonObject.get("mincount"));
		int maxcount = Integer.parseInt((String) jsonObject.get("maxcount"));
		String keyname = (String) jsonObject.get("keyname");
		String securitygroup = (String) jsonObject.get("securitygroup");

		RunInstancesRequest runInstancesRequest = new RunInstancesRequest();

		runInstancesRequest.withImageId(image).withInstanceType(instancetype)
				.withMinCount(mincount).withMaxCount(maxcount)
				.withKeyName(keyname).withSecurityGroups(securitygroup);

		RunInstancesResult runInstancesResult = ec2
				.runInstances(runInstancesRequest);


		AmazonEC2Instance amazonInstance = new AmazonEC2Instance();
		amazonInstance.setInstanceId(runInstancesResult.getReservation()
				.getInstances().get(0).getInstanceId());
		amazonInstance.setImageId(runInstancesResult.getReservation()
				.getInstances().get(0).getImageId());
		amazonInstance.setInstanceIP(runInstancesResult.getReservation()
				.getInstances().get(0).getPublicIpAddress());

		int timeout = TIMEOUTPERIOD;
		int status = 1;
		System.out.print("Bringing up instance. Please wait ...");
		while (!getInstanceState(amazonInstance).equals("running")) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			timeout++;
			if (timeout == TIMEOUTPERIOD) {
				status = -1;
				break;
			}
		}
		if (status != -1) {
			middlewareInstanceHash.put(amazonInstance.getInstanceId(), amazonInstance);

			System.out.println("Bringing up Instance "
					+ amazonInstance.getInstanceId() + " successful.");
			return amazonInstance;
		} else {
			System.out
					.println("Bringing up Instance failed. Please check the status after some time.");
			return null;
		}

	}

	@Override
	public void describeAllInstances() {
		// System.out.println(ec2.describeInstances());
		for(String id: middlewareInstanceHash.keySet()) {
			System.out.println(middlewareInstanceHash.get(id));
		}
	}

	@Override
	public void pauseInstance(Instance ourInstance) {

		List<String> instanceList = new ArrayList<String>();
		instanceList.add(ourInstance.getInstanceId());
		ec2.stopInstances(new StopInstancesRequest(instanceList));
		int timeout = 0;
		int status = 1;
		System.out.print("Pausing " + ourInstance.getInstanceId()
				+ ". Please wait ...");
		while (!getInstanceState(ourInstance).equals("stopped")) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			timeout++;
			if (timeout == TIMEOUTPERIOD) {
				status = -1;
				break;
			}
		}
		if (status != -1) {
			AmazonEC2Instance tempInstance = (AmazonEC2Instance) ec2InstanceMap
					.get(ourInstance.getInstanceId());
			if (tempInstance != null) {
				tempInstance.setInstanceState(getInstanceState(ourInstance));
				tempInstance.setInstanceIP(null);
				ec2InstanceMap.put(ourInstance.getInstanceId(), tempInstance);
			}
			System.out.println("Pause successful!");
		} else {
			System.out
					.println("Pause failed! Please check the status after some time");
		}
	}

	@Override
	public void resumeInstance(Instance ourInstance) {
		// TODO Auto-generated method stub
		List<String> instanceList = new ArrayList<String>();
		instanceList.add(ourInstance.getInstanceId());
		ec2.startInstances(new StartInstancesRequest(instanceList));
		int timeout = 0;
		int status = 1;
		System.out.print("Resuming " + ourInstance.getInstanceId()
				+ ". Please wait ...");
		while (!getInstanceState(ourInstance).equals("running")) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			timeout++;
			if (timeout == TIMEOUTPERIOD) {
				status = -1;
				break;
			}
		}
		if (status != -1) {
			AmazonEC2Instance tempInstance = (AmazonEC2Instance) middlewareInstanceHash
					.get(ourInstance.getInstanceId());
			if (tempInstance != null) {
				tempInstance.setInstanceState(getInstanceState(ourInstance));
				/*
				 * We have to find the Public IP Address after the instance is
				 * resumed. There is a chance it might receive a new IP Address
				 */
				for (int i = 0; i < ec2.describeInstances().getReservations()
						.size(); i++) {
					for (int j = 0; j < ec2.describeInstances()
							.getReservations().get(i).getInstances().size(); j++) {
						if (ec2.describeInstances().getReservations().get(i)
								.getInstances().get(j).getInstanceId()
								.equals(ourInstance.getInstanceId()))
							tempInstance.setInstanceIP(ec2.describeInstances()
									.getReservations().get(i).getInstances()
									.get(j).getPublicIpAddress());
					}
				}
				middlewareInstanceHash.put(ourInstance.getInstanceId(), tempInstance);
			}
			System.out.println("Resume successful!");
		} else {
			System.out
					.println("Resume failed! Please check the status after some time");
		}
	}

	@Override
	public void terminateInstance(Instance ourInstance) {
		// TODO Auto-generated method stub
		List<String> instanceList = new ArrayList<String>();
		instanceList.add(ourInstance.getInstanceId());
		ec2.terminateInstances(new TerminateInstancesRequest(instanceList));

		int timeout = 0;
		int status = 1;
		System.out.print("Terminating " + ourInstance.getInstanceId()
				+ ". Please wait ...");
		while (!getInstanceState(ourInstance).equals("terminated")) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			timeout++;
			if (timeout == TIMEOUTPERIOD) {
				status = -1;
				break;
			}
		}
		if (status != -1) {
			AmazonEC2Instance tempInstance = (AmazonEC2Instance) middlewareInstanceHash
					.get(ourInstance.getInstanceId());
			if (tempInstance != null) {
				tempInstance.setInstanceState(getInstanceState(ourInstance));

				/*
				 * We have to remove the instance after terminating it
				 */
				middlewareInstanceHash.remove(ourInstance.getInstanceId());
			}
			System.out.println("Terminate successful!");
		} else {
			System.out
					.println("Terminate failed! Please check the status after some time");
		}
	}

	public String getInstanceState(Instance ourInstance) {
		String state = "";
		/*
		 * States ------ 0 : pending 16 : running 32 : shutting-down 48 :
		 * terminated 64 : stopping 80 : stopped
		 */
		for (int i = 0; i < ec2.describeInstances().getReservations().size(); i++) {
			for (int j = 0; j < ec2.describeInstances().getReservations()
					.get(i).getInstances().size(); j++) {
				if (ec2.describeInstances().getReservations().get(i)
						.getInstances().get(j).getInstanceId()
						.equals(ourInstance.getInstanceId())) {
					state = ec2.describeInstances().getReservations().get(i)
							.getInstances().get(j).getState().getName();
					break;
				}
			}
		}
		return state;
	} // End of getInstanceState

	@Override
	public void listAllImages() {
		// TODO Auto-generated method stub
		System.out.println(ec2.describeImages());
	}

	@Override
	public void createKeyPair(String name) {
		// TODO Auto-generated method stub
		ec2.createKeyPair(new CreateKeyPairRequest(name));
	}

	@Override
	public void listAllVolumes() {
		// TODO Auto-generated method stub
		System.out.println(ec2.describeVolumes());
	}

	@Override
	public String getVolumeState(Volume ourVolume) {
		String state = "";
		/*
		 * States : creating, available, in-use, deleting, deleted, error
		 */
		for (int i = 0; i < ec2.describeVolumes().getVolumes().size(); i++) {
			if (ec2.describeVolumes().getVolumes().get(i).getVolumeId()
					.equals(ourVolume.getVolumeId())) {
				state = ec2.describeVolumes().getVolumes().get(i).getState();
				break;
			}
		}
		return state;
	} // End of getInstanceState

	@Override
	public Volume createVolume(String args) {
		// TODO Auto-generated method stub
		Object obj = JSONValue.parse(args);
		JSONObject jsonObject = (JSONObject) obj;
		int size = Integer.valueOf((String) jsonObject.get("size"));
		String availabilityZone = (String) jsonObject.get("availabilityzone");
		CreateVolumeRequest createvolume = new CreateVolumeRequest(size,
				availabilityZone);
		CreateVolumeResult result = ec2.createVolume(createvolume);
		AmazonEC2Volume ourVolume = new AmazonEC2Volume();
		ourVolume.setVolumeId(result.getVolume().getVolumeId());
		ourVolume.setVolumeSize(String.valueOf(size));
		return ourVolume;

	}

	@Override
	public void deleteVolume(Volume ourVolume) {
		// TODO Auto-generated method stub
		ec2.deleteVolume(new DeleteVolumeRequest(ourVolume.getVolumeId()));

	}

	@Override
	public void attachVolume(Volume ourVolume, Instance ourInstance,
			String deviceName) {
		// TODO Auto-generated method stub
		AttachVolumeRequest AVR = new AttachVolumeRequest(
				ourVolume.getVolumeId(), ourInstance.getInstanceId(),
				deviceName);
		ec2.attachVolume(AVR);

	}

	@Override
	public void detachVolume(Volume ourVolume) {
		// TODO Auto-generated method stub
		ec2.detachVolume(new DetachVolumeRequest(ourVolume.getVolumeId()));

	}

	@Override
	public String getAvailabilityZone(Instance ourInstance) {
		String availabilityZone = "";

		for (int i = 0; i < ec2.describeInstances().getReservations().size(); i++) {
			for (int j = 0; j < ec2.describeInstances().getReservations()
					.get(i).getInstances().size(); j++) {
				if (ec2.describeInstances().getReservations().get(i)
						.getInstances().get(j).getInstanceId()
						.equals(ourInstance.getInstanceId())) {
					availabilityZone = ec2.describeInstances()
							.getReservations().get(i).getInstances().get(j)
							.getPlacement().getAvailabilityZone();
					break;
				}
			}
		}
		return availabilityZone;

		// TODO Auto-generated method stub

	}

	@Override
	public String deployApplication(String inputDetails, String instanceID) {
		// TODO Auto-generated method stub

		// /opt/stack/Downloads/Lecture4.pdf cirros@10.0.0.2:~/

		DeployApp scp = new DeployApp(inputDetails, instanceID);
		scp.scp();

		return null;
	}

	@Override
	public Instance getInstance(String instanceId) {
		// TODO Auto-generated method stub

		return middlewareInstanceHash.get(instanceId);
	}

	@SuppressWarnings("deprecation")
	@Override
	public Instance importInstance(String input) {
		// TODO Auto-generated method stub
		// Create S3 client object to get the manifest URL
		String importInstanceResult = null;
		String instanceId = "";
		AmazonEC2Instance amazonInstance = new AmazonEC2Instance();
		try {
			FlavorMap f = new FlavorMap();
			Object obj = JSONValue.parse(input);
			JSONObject jsonObject = (JSONObject) obj;
			String importMode = (String) jsonObject.get("mode");
			String bucket_name = (String) jsonObject.get("bucket");
			String flavor = f.amazonFlavorMap(input);
			//System.out.println("Flavor for the instance is " + flavor);

			if (importMode.equals("remote")) {
				String manifest_file = (String) jsonObject.get("manifest");
				String architectureTypeFromJSON = (String) jsonObject
						.get("architecture");
				String virtualDiskTypeFromJSON = (String) jsonObject
						.get("format");
				String flavorValueFromJSON = flavor;
				String platformValueFromJSON = (String) jsonObject
						.get("platform");

				AmazonS3Driver sample = new AmazonS3Driver(bucket_name,
						manifest_file, credentials);
				String manifestUrl = sample.getPreSignedUrl();
				// launch specifications(-t instance type, -a architecture)
				ImportInstanceRequest imp = new ImportInstanceRequest();
				ImportInstanceLaunchSpecification imp_ls = new ImportInstanceLaunchSpecification();
				imp_ls.setArchitecture(architectureTypeFromJSON); // x86_64
				imp_ls.setInstanceType(flavorValueFromJSON); // m3.medium
				imp.setLaunchSpecification(imp_ls);
				imp.setPlatform(platformValueFromJSON);

				// disk image format specifications
				DiskImage dimg = new DiskImage();

				DiskImageDetail dimg_detail = new DiskImageDetail();
				long img_size = 3551363584L;
				dimg_detail.setFormat(virtualDiskTypeFromJSON);
				dimg_detail.setBytes(img_size);

				long vol_size = 8L;
				VolumeDetail vol_detail = new VolumeDetail();
				vol_detail.setSize(vol_size);

				// dimg_detail = dimg_detail.withImportManifestUrl(manifestUrl);
				dimg_detail.setImportManifestUrl(manifestUrl);
				dimg.setImage(dimg_detail);
				dimg.setVolume(vol_detail);

				imp.withDiskImages(dimg);

				importInstanceResult = (ec2.importInstance(imp)).toString();
			}

			else if (importMode.equals("local")) {

				String virtualDiskPathFromJSON = (String) jsonObject
						.get("path");
				String architectureTypeFromJSON = (String) jsonObject
						.get("architecture");
				String virtualDiskTypeFromJSON = (String) jsonObject
						.get("format");
				String flavorValueFromJSON = flavor;
				String platformValueFromJSON = (String) jsonObject
						.get("platform");
				//String regionFromJSON = (String) jsonObject.get("region");
				String regionFromJSON = "us-west-2";
				
				String ec2ApiToolsPath = "/opt/stack/Documents/ec2-api-tools-1.6.13.0/bin/";
				String ec2Command = "ec2-import-instance";
				String virtualDiskPath = virtualDiskPathFromJSON;
				String flavorValue = flavorValueFromJSON;
				String virtualDiskType = virtualDiskTypeFromJSON;
				String architectureType = architectureTypeFromJSON;
				String accessKey = credentials.getAWSAccessKeyId().replace(
						"\n", "");
				String secretKey = credentials.getAWSSecretKey().replace("\n",
						"");
				// String bucketOwnerAccessKey = "AKIAIZKAY7ZVB4ZGQDPQ";
				// String bucketOwnerSecretKey =
				// " zTM/T6R055e8ibe45C6OqsB1DlfhAW4Tq10tj8WD";
				String bucketOwnerAccessKey = accessKey;
				String bucketOwnerSecretKey = secretKey;
				String bucketName = bucket_name;
				String platform = platformValueFromJSON;
				StringBuffer processCmd = new StringBuffer();
				processCmd.append(ec2ApiToolsPath);
				processCmd.append(ec2Command);
				processCmd.append(" " + virtualDiskPath);
				processCmd.append(" -t ");
				processCmd.append(flavorValue);
				processCmd.append(" -f ");
				processCmd.append(virtualDiskType);
				processCmd.append(" -a ");
				processCmd.append(architectureType);
				processCmd.append(" -O ");
				processCmd.append(accessKey);
				processCmd.append(" -W ");
				processCmd.append(secretKey);
				processCmd.append(" -o ");
				processCmd.append(bucketOwnerAccessKey);
				processCmd.append(" -w ");
				processCmd.append(bucketOwnerSecretKey);
				processCmd.append(" -b ");
				processCmd.append(bucketName);
				processCmd.append(" -p ");
				processCmd.append(platform);
				processCmd.append(" --region ");
				processCmd.append(regionFromJSON);

				ProcessBuilder pb = new ProcessBuilder("/bin/bash", "-l", "-c",
						processCmd.toString());
				pb.environment().put("EC2_HOME",
						"/opt/stack/Documents/ec2-api-tools-1.6.13.0/");
				pb.environment().put("JAVA_HOME",
						"/usr/lib/jvm/java-1.6.0-openjdk-amd64");
				System.out.println("Image is being uploaded... Please Wait...");
				Process p = pb.start();

				int exitVal = p.waitFor();

				InputStreamReader isr = new InputStreamReader(
						p.getInputStream());
				BufferedReader br = new BufferedReader(isr);

				String line, conversionTaskId;
				String region;
				conversionTaskId = null;

				// Thread.sleep(15000);
				int success1 = 0, success2 = 0;
				while ((line = br.readLine()) != null) {
					int counter = 0;
					if (line.startsWith("Done"))
						success1 = 1;
					String contentsArray[] = line.split("\\s+");

					if (conversionTaskId == null) {
						for (String i : contentsArray) {
							if (i.trim().startsWith("import-i"))
								conversionTaskId = i;
							if (i.trim().startsWith("i-"))
								instanceId = i;
							counter++;
						}
					}

					if (line.contains("has been uploaded")) {
						success2 = 1;
					}
					// System.out.println(line);

				}
				// int exitVal = p.waitFor();

				// System.out.println(processCmd);

				if (exitVal == 0) {
					System.out.println("Instance Upload to S3 Succesful...");
					System.out.println("Conversion Task Id = " + conversionTaskId);
					System.out.println("Instance Id = " + instanceId);
					System.out.println("Please wait till the instance is created...");
				} else {
					System.out.println("Invalid Input parameters !!!");
				}
				
				boolean isCompleted = (success1 == 1) && (success2 == 1);

				if (isCompleted) {

					DescribeConversionTasksResult conversionResult;

					boolean canStart = false;
					boolean isCancelled = false;
					while (canStart != true && isCancelled != true) {
						conversionResult = ec2.describeConversionTasks();
						for (int i = 0; i < conversionResult
								.getConversionTasks().size(); i++) {
							if (conversionResult.getConversionTasks().get(i)
									.getConversionTaskId()
									.equals(conversionTaskId)
									&& conversionResult.getConversionTasks()
											.get(i).getState()
											.equals("completed")) {
								// Completed && We can start the instance
								canStart = true;
							}
							if (conversionResult.getConversionTasks().get(i)
									.getConversionTaskId()
									.equals(conversionTaskId)
									&& conversionResult.getConversionTasks()
											.get(i).getState()
											.equals("cancelled")) {
								isCancelled = true;
							}
						}
						try {
							Thread.sleep(5000);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					if (canStart) {
						System.out.println("Starting Instance...");
						StartInstancesRequest startRequest = new StartInstancesRequest()
								.withInstanceIds(instanceId);
						ec2.startInstances(startRequest);
					}
				}

			} // End of Else if Local Mode Import Instance

			amazonInstance.setInstanceId(instanceId);
			amazonInstance.setProviderName("AMAZON");
			for (int i = 0; i < ec2.describeInstances().getReservations()
					.size(); i++) {
				for (int j = 0; j < ec2.describeInstances().getReservations()
						.get(i).getInstances().size(); j++) {
					if (ec2.describeInstances().getReservations().get(i)
							.getInstances().get(j).getInstanceId()
							.equals(instanceId)) {
						amazonInstance.setInstanceIP(ec2.describeInstances()
								.getReservations().get(i).getInstances().get(j)
								.getPublicIpAddress());
						amazonInstance.setInstanceState(ec2.describeInstances()
								.getReservations().get(i).getInstances().get(j)
								.getState().getName());
					}
				}
			}
			ec2InstanceMap.put(amazonInstance.getInstanceId(), amazonInstance);
			middlewareInstanceHash.put(amazonInstance.getInstanceId(), amazonInstance);
		} // End of Try
		catch (Exception e) {
			e.printStackTrace();
		}
		
		/*
		 * Associating Security Group with Port 22 ingress permissions
		 */
		String groupId = this.createSecurityGroup("SG_" + amazonInstance.getInstanceId());
		this.associateSecurityGroupWithInstance(groupId, amazonInstance);
		
		return amazonInstance;
	} // End of Import Instance

	@Override
	public String exportInstance(String input) {
		// TODO Auto-generated method stub
		CreateInstanceExportTaskRequest exportTaskRequest = new CreateInstanceExportTaskRequest();

		ExportToS3TaskSpecification exportToS3Task = new ExportToS3TaskSpecification();

		String diskImageFormat = "vhd";
		exportToS3Task.setDiskImageFormat(diskImageFormat);
		String s3Bucket = "iaasmiddleware";
		exportToS3Task.setS3Bucket(s3Bucket);

		exportTaskRequest.setExportToS3Task(exportToS3Task);
		exportTaskRequest.setInstanceId(input);
		String targetEnv = "microsoft";
		exportTaskRequest.setTargetEnvironment(targetEnv);

		CreateInstanceExportTaskResult exportRetValue = new CreateInstanceExportTaskResult();
		exportRetValue = ec2.createInstanceExportTask(exportTaskRequest);

		String exportTaskId = exportRetValue.getExportTask().getExportTaskId();
		String exportStatus = exportRetValue.getExportTask().getState();

		System.out.println("Export Task Id:" + exportTaskId);
		//System.out.println(exportStatus);

		while (!exportStatus.equalsIgnoreCase("completed")) {

			DescribeExportTasksRequest d = new DescribeExportTasksRequest();
			DescribeExportTasksResult eTaskRes = ec2.describeExportTasks(d
					.withExportTaskIds(exportTaskId));
			exportStatus = eTaskRes.getExportTasks().get(0).getState();
			// System.out.println(exportStatus);
			/*
			 * for(int i = 0; i <
			 * ec2.describeExportTasks().getExportTasks().size() ; i++){
			 * System.out.println(i);
			 * if(ec2.describeExportTasks().getExportTasks
			 * ().get(i).getExportTaskId().equals(exportTaskId)) { exportStatus
			 * = ec2.describeExportTasks().getExportTasks().get(i).getState();
			 * System.out.println(i + " " +exportTaskId + " " + exportStatus); }
			 * }
			 */
			try {
				Thread.sleep(5000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		String exportDetails = "{"
				+ "\"bucketName\" : \""
				+ exportRetValue.getExportTask().getExportToS3Task()
						.getS3Bucket().toString()
				+ "\","
				+ "\"S3Key\" : \""
				+ exportRetValue.getExportTask().getExportToS3Task().getS3Key()
						.toString() + "\"" + "}";

		return exportDetails;
	}

	public AWSCredentials getCredentials() {
		// TODO Auto-generated method stub
		/*
		 * String amazonCredentials = "{" + "\"provider\" : \"Amazon\"," +
		 * "\"accessKey\" : \""+ credentials.getAWSAccessKeyId()+"\"," +
		 * "\"secretKey\" : \""+ credentials.getAWSSecretKey() +"\"" + "}";
		 */
		return credentials;
	}

	@Override
	public boolean resizeInstance(Instance instance, String flavorDetails) {

		// TODO Auto-generated method stub
		try {

			pauseInstance(instance);
			FlavorMap f = new FlavorMap();
			String flavor = f.amazonFlavorMap(flavorDetails);
			ModifyInstanceAttributeRequest modify = new ModifyInstanceAttributeRequest();
			modify.withInstanceId(instance.getInstanceId()).setInstanceType(
					flavor);
			ec2.modifyInstanceAttribute(modify);
			resumeInstance(instance);
			System.out.println("Instance has been resized successfully .....");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public String createSecurityGroup(String input) {
		/*
		 * Creates a Secruity Group with a given name
		 * By default gives associates SSH with to it
		 */
		String securityGroupName = input; // This instance id is obtained from the input argument
		CreateSecurityGroupRequest createSecurityGroupRequest = new CreateSecurityGroupRequest();
		createSecurityGroupRequest.withGroupName(securityGroupName).withDescription("Security group for " + securityGroupName);
		CreateSecurityGroupResult createSecurityGroupResult = ec2.createSecurityGroup(createSecurityGroupRequest);
		System.out.println("Created Security Group "+ securityGroupName+" Successfully: " + createSecurityGroupResult.toString());
		
		// Authorize the Security Group for ingress port 22 and ICMP 
		
		IpPermission ipPermission = new IpPermission();
		ipPermission.withIpRanges("0.0.0.0/0")
        .withIpProtocol("tcp")
        .withFromPort(22)
        .withToPort(22);
		
		IpPermission ipPermission1 = new IpPermission(); 
		ipPermission1.withIpRanges("0.0.0.0/0")
		.withIpProtocol("icmp")
		.withFromPort(-1)
		.withToPort(-1);
		
		AuthorizeSecurityGroupIngressRequest authorizeSecurityGroupIngressRequest = new AuthorizeSecurityGroupIngressRequest();
		authorizeSecurityGroupIngressRequest.withGroupName(securityGroupName)
											.withIpPermissions(ipPermission, ipPermission1);
		ec2.authorizeSecurityGroupIngress(authorizeSecurityGroupIngressRequest);
		
		return createSecurityGroupResult.getGroupId();
		
	}

	@Override
	public void associateSecurityGroupWithInstance(String securityGroupName,
			Instance instanceId) {
		// TODO Auto-generated method stub
		
		//ModifyInstanceAttributeRequest modifyInstanceAttributeRequest = new ModifyInstanceAttributeRequest(instanceId.getInstanceId(), InstanceAttributeName.GroupSet);
		ModifyInstanceAttributeRequest modifyInstanceAttributeRequest = new ModifyInstanceAttributeRequest();
		modifyInstanceAttributeRequest.withGroups(securityGroupName).withInstanceId(instanceId.getInstanceId());
		ec2.modifyInstanceAttribute(modifyInstanceAttributeRequest);
		System.out.println("Security Group "+securityGroupName+" associated with Instance "+instanceId.getInstanceId()+" successfully! ");
	}
}