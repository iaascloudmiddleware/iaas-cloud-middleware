package com.iaascloud.compute;

import java.io.IOException;

import com.iaascloud.compute.drivers.AmazonEC2Driver;
import com.iaascloud.compute.drivers.OpenstackDriver;
import com.iaascloud.compute.drivers.ProviderDriver;

public class Initializer{
	private final Providers name;
	private String inputDetails;  
	
	public Initializer(Providers name, String inputDetails) { 
		this.name = name;
		//this.credentialsFilePath = filePath; 
		this.inputDetails = inputDetails; 
	}
	
	public ProviderDriver getDriver() throws Exception, IOException, InterruptedException{
		if(name == Providers.AMAZON) {
			AmazonEC2Driver pDriver = new AmazonEC2Driver(inputDetails);
			return pDriver;
		} else {
			OpenstackDriver pDriver = new OpenstackDriver(inputDetails);
			return pDriver;
		} 
	}
	
	public Providers getProviderName() {
		return name; 
	}
}
