package com.iaascloud.compute.volumes;

public class AmazonEC2Volume extends Volume {

	String volumeId;
	String volumeSize;
	String volumeName;

	public AmazonEC2Volume()
	{
		volumeId = "";
		volumeSize = "";
		volumeName = "";
	}
	
	@Override
	public String getVolumeId() {
		// TODO Auto-generated method stub
		return volumeId;
	}

	@Override
	public String getVolumeName() {
		// TODO Auto-generated method stub
		return volumeName;
	}

	@Override
	public String getVolumeSize() {
		// TODO Auto-generated method stub
		return volumeSize;
	}
	
	public void setVolumeId(String volumeId)
	{
		this.volumeId = volumeId;
	}
	public void setVolumeName(String volumeName)
	{
		this.volumeName = volumeName;
	}
	public void setVolumeSize(String volumeSize)
	{
		this.volumeSize = volumeSize;
	}
}
