package com.iaascloud.compute.volumes;

public abstract class Volume {
	public abstract String getVolumeId();
	public abstract String getVolumeName();
	public abstract String getVolumeSize(); 
}
