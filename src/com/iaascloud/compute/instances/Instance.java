package com.iaascloud.compute.instances;

public abstract class Instance {
	public abstract String getInstanceId(); 
	public abstract String getInstanceName();
	public abstract String getImageId();   
	public abstract String toString(); 
	public abstract String getUserName();
	public abstract String getInstanceIP();
	public abstract String getInstanceState(); 
	public abstract String getProviderName(); 
	/*public abstract void setInstanceId(String instanceId); 
	public abstract void setInstanceName(String instanceName); 
	public abstract void setImageId(String imageId);
	public abstract void setVolumeId(String volumeId);*/
}
