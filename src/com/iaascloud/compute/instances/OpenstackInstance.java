package com.iaascloud.compute.instances;

public class OpenstackInstance extends Instance{

	private String instanceId;
	private String imageId;
	private String instanceName;
	private String instanceIP;
	private String userName;
	private String instanceState;
	private String providerName;
	
	public OpenstackInstance() {
		// TODO Auto-generated constructor stub
		instanceId = "";
		instanceName = "";
		imageId = "";
		instanceIP = "";
		userName = "";
		providerName = "";
	}
	
	@Override
	public String getInstanceId() {
		// TODO Auto-generated method stub
		return instanceId;
	}

	@Override
	public String getInstanceName() {
		// TODO Auto-generated method stub
		return instanceName;
	}

	@Override
	public String getImageId() {
		// TODO Auto-generated method stub
		return imageId;
	}

	public String getInstanceIP() {
		return instanceIP;
	}

	public String getUserName() {
		return userName;
	}
	public void setInstanceId(String instanceId) {
		// TODO Auto-generated method stub
		this.instanceId = instanceId;
	}

	public void setInstanceName(String instanceName) {
		// TODO Auto-generated method stub
		this.instanceName = instanceName;
	}

	public void setImageId(String imageId) {
		// TODO Auto-generated method stub
		this.imageId = imageId;
	}

	public void setInstanceIP(String instanceIP) {
		this.instanceIP = instanceIP;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Override
	public String toString() {
		String returnString = "[Instance Id = "+getInstanceId()+" , Instance Name = "+getInstanceName()+" , Image Id = "+getImageId()+", Instance State ="+getInstanceState()+" , Instance IP Address = "+getInstanceIP()+" ,Providername = "+getProviderName()+" ]"; 
		return returnString;
	}

	@Override
	public String getInstanceState() {
		// TODO Auto-generated method stub
		return instanceState;
	}
	
	public void setInstanceState(String instanceState) {
		this.instanceState = instanceState; 
	}

	@Override
	public String getProviderName() {
		// TODO Auto-generated method stub
		return providerName;
	}
	public void setProviderName(String providerName)
	{
		this.providerName = providerName;
	}
}
