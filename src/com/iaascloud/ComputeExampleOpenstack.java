package com.iaascloud;

import java.io.IOException;

import com.iaascloud.compute.Initializer;
import com.iaascloud.compute.Providers;
import com.iaascloud.compute.drivers.OpenstackDriver;
import com.iaascloud.compute.drivers.ProviderDriver;
import com.iaascloud.compute.instances.Instance;

public class ComputeExampleOpenstack {

	public static void main(String[] args) throws IOException, InterruptedException, Exception {
		/*
		 * Initial Details for Openstack
		 */
		
		String initialDetailsOpenstack = 
				"{"
				+ "\"username\" : \"admin\","
				+ "\"password\" : \"123\""
				+ "\"authurl\" : \"http://localhost\""
				+ "\"computeurl\": \"http://192.168.0.100:8774/v2\""
				+ "}"; 
		
		Initializer ourInitializer = new Initializer(Providers.OPENSTACK, initialDetailsOpenstack); 
		ProviderDriver ourDriver = ourInitializer.getDriver();
		ourDriver.describeAllInstances();
		
		/*String instanceCreateOpenstack = 
				"{" 
				+ "\"imageid\": \"56d4de87-7e41-44a3-8690-e11ec91b1568\","
				+ "\"flavorid\": \"2\","
				+ "\"mincount\": \"1\","
				+ "\"maxcount\": \"1\","
				+ "\"instancename\": \"venky\","
				+ "\"computeurl\": \"http://192.168.0.100:8774/v2\""
				+ "}"; 
		Instance openstackInstance = ourDriver.createInstance(instanceCreateOpenstack);
		System.out.println(openstackInstance);*/
		
		String args1 = 
				"{" 
				+ "\"name\": \"Ubuntu_OS_ex\","
				+ "\"mode\": \"local\","
				+ "\"diskformat\": \"vhd\","
				+ "\"mindisk\": \"8\","
				+ "\"minram\": \"1024\","
				+ "\"architecture\": \"x86_64\","
				+ "\"filepath\": \"/opt/stack/Documents/Ubuntu_AWS/Ubuntu_AWS.vhd\""
				+ "}";
		
		//+ "\"filepath\": \"/opt/stack/Documents/Ubuntu_AWS/Ubuntu_AWS.vhd\""
		
		//Instance a = ourDriver.importInstance(args1);
		//System.out.println(a);
		//ourDriver.resizeInstance("21776bc5-2a9b-4db6-be90-63b7d637cdbf", args1);
		
		//EXPORT in OPEN STACK
		
		
		
		/*String imageName = "Ubuntu_OS_instance_Snapshot1";
		String instanceID = "24f299aa-4e9a-4bf3-bf71-86669d86b8fe";
		
		String exportInstance =
				"{" 
						+ "\"instanceid\": \""+instanceID+"\","
						+ "\"imagename\": \""+imageName+"\","
						+ "}";
		
		
		String SourceRAW = "Source_1.raw";
		String vmdkFileName ="Destination_1.vmdk";
		String vhdFileName ="Destination_2.vhd";
		
		
		
		 
		 OpenstackDriver  o = new OpenstackDriver(initialDetailsOpenstack);
		ourDriver.exportInstance(exportInstance);
		
		//Wait until completion
		o.waitforSnapShotCompletion(imageName);
		
		//Take ID of the SNAP SHOT
		
		String snapID= o.extractSnapID(imageName);
		o.downloadRAW(SourceRAW, snapID);
		o.convertToVMDK(SourceRAW, vmdkFileName);
		o.convertToVHD(vmdkFileName, vhdFileName);*/
		
		

	}

}