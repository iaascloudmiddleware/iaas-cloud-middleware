package com.iaascloud;

import java.io.IOException;

import com.iaascloud.compute.Initializer;
import com.iaascloud.compute.Providers;
import com.iaascloud.compute.drivers.ProviderDriver;
import com.iaascloud.compute.instances.Instance;
import com.iaascloud.compute.volumes.Volume;
import com.iaascloud.deploy.SSH_To_Instance;

public class ComputeExampleEC2 {

	public static void main(String[] args) throws Exception, IOException, InterruptedException{
		/*
		 * Initial Details for Amazon
		 */
		
		String initialDetailsAmazon = 
				"{"
				+ "\"keypath\" : \"/opt/stack/projecta/iaas-cloud-middleware/src/AwsCredentials.properties\","
				+ "\"endpoint\" : \"us-west-2\""
				+ "}"; 
		Initializer ourInitializer = new Initializer(Providers.AMAZON, initialDetailsAmazon); 
		ProviderDriver ourDriver = ourInitializer.getDriver();
		
		/*
		 * Import Instance JSON using an already imported instance using a Manifest.xml
		 */
		
		String importInstanceJSONUsingManifest = 
				"{"
				+ "\"mode\" : \"remote\""
				+ "\"bucket\" : \"iaasmiddleware\""
				+ "\"manifest\" : \"d5e9e89d-6068-41b1-acec-122d36ba37bf/Ubuntu_AWS.vhdmanifest.xml\""
				+ "\"architecture\" : \"x86_64\""
				+ "\"mindisk\": \"8\","
				+ "\"minram\": \"1024\","
				+ "\"format\" : \"VHD\""
				+ "\"platform\" : \"Linux\""
				+ "}";
		
		/*
		 * Import Instance JSON using an image file that exists in the local filesystem
		 */
		
		String importInstanceJSON = 
				"{"
				+ "\"mode\" : \"local\""
				+ "\"bucket\" : \"iaasmiddleware\""
				+ "\"path\" : \"/opt/stack/projecta/iaas-cloud-middleware/6fc68347-f54a-4860-933f-c9bc776ed31d_VHD.vhd\""
				+ "\"architecture\" : \"x86_64\""
				+ "\"mindisk\": \"8\","
				+ "\"minram\": \"2048\","
				+ "\"format\" : \"VHD\""
				+ "\"platform\" : \"Linux\""
				+ "\"region\" : \"us-west-2\""
				+ "}";
		
		//System.out.println(ourDriver.importInstance(importInstanceJSON));
		
		ourDriver.resizeInstance(ourDriver.getInstance("i-f008e3fb"), importInstanceJSON);
		
		SSH_To_Instance ssh = new SSH_To_Instance();
		//ssh.SSHToInstance(username, instanceId)
		
		//MigrateDriver mig = new MigrateDriver(Providers.AMAZON, ourDriver, )
		
		/*
		 * Export an instance already imported to amazon
		 */
		//String res = ourDriver.exportInstance("dummy");
		//System.out.println(res);
		
		
		
		
		
		/*Instance myInstance = ourDriver.getInstance("i-aea6dda6");
		ourDriver.pauseInstance(myInstance);
		System.out.println(myInstance);*/
		
		/*String instanceCreate = 
				"{" 
				+ "\"image\": \"ami-ccf297fc\","
				+ "\"instancetype\": \"t1.micro\","
				+ "\"mincount\": \"1\","
				+ "\"maxcount\": \"1\","
				+ "\"keyname\": \"venky\","
				+ "\"securitygroup\": \"default\""
				+ "}"; 
		
		Instance ourInstance = ourDriver.createInstance(instanceCreate);
		System.out.println(ourInstance);
		
		/*
		 * IMPORTANT (FOR EC2) 
		 * 
		 * Availability zone for instance and for volume must be same
		 * 
		 */
		
		/*String volumeString = 
				"{" 
						+ "\"size\": \"8\","
						+ "\"availabilityzone\": \""+ourDriver.getAvailabilityZone(ourInstance)+"\","
				+ "}";
		Volume ourVolume = ourDriver.createVolume(volumeString);
		System.out.println(ourDriver.getVolumeState(ourVolume));
		
		ourDriver.attachVolume(ourVolume, ourInstance, "/dev/sda3");*/
	}
}
