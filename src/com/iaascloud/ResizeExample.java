package com.iaascloud;

import java.io.IOException;

import com.iaascloud.compute.Initializer;
import com.iaascloud.compute.Providers;
import com.iaascloud.compute.drivers.ProviderDriver;
import com.iaascloud.compute.instances.Instance;

public class ResizeExample {

	public static void main(String args[]) throws IOException, InterruptedException, Exception
	{
		String credentialsDetails = 
				"{"
				+ "\"keypath\" : \"/opt/stack/projecta/iaas-cloud-middleware/src/AwsCredentials.properties\","
				+ "\"endpoint\" : \"us-west-2\""
				+ "\"username\" : \"admin\","
				+ "\"password\" : \"123\""
				+ "\"authurl\" : \"http://192.168.0.100\""
				+ "}"; 
		
		Initializer ourInitializer = new Initializer(Providers.AMAZON, credentialsDetails); 
		ProviderDriver ourDriver = ourInitializer.getDriver();
		
		Initializer newInitializer = new Initializer(Providers.OPENSTACK, credentialsDetails); 
		ProviderDriver newDriver = newInitializer.getDriver();
		
		
		
		Instance ourInstance = ourDriver.getInstance("i-c53692cd");
		String flavorDetails = "{"
				+ "\"mindisk\": \"8\","
				+ "\"minram\": \"2048\","
								+ "}";
		ourDriver.resizeInstance(ourInstance, flavorDetails);
	}
}
